
#include <iostream>
#include <vector>
#include <string>

#include "optionparser.h"
#include "factory.h"

/******************************************************************************
 * An example custom data type
 */
struct CustomClass {
    std::string s;
    int l;
};

// To be able to use custom datatypes as arguments, reimplement convertToType
// for the specific type, or overload stream operator for std::stringstream.
namespace Option {
template <>
inline CustomClass convertToType<CustomClass>(std::string s)
{
    CustomClass obj;
    std::stringstream ss(s);
    ss >> obj.s;
    obj.l = s.length();
    return obj;
}
}



/******************************************************************************
 * A custom data type with a method to add options to OptionParser
 */
class CustomClass2 {
public:
    CustomClass2(int i) : integer(i) {}
    static int id;
    static void addOption(std::vector<CustomClass2*>* objectList,
                                     OptionParser *op) {
        op->add(new Option::Multiple<CustomClass2*, true>(
                    objectList, "custom2", '2',
                    "Adds a Custom class via Factory"));
    }
    int integer;
};

// Register CustomClass2 at Factory as a constructable class
int CustomClass2::id =
        Factory<CustomClass2>::registerClass(&(CustomClass2::addOption));

// Custom typeconverter for CustomClass2
namespace Option {
template <>
inline CustomClass2* convertToType<CustomClass2*>(std::string s)
{
    return new CustomClass2(convertToType<int>(s));
}
}



/******************************************************************************
 * Example main
 */
int main(int argc, char* argv[])
{
    if (argc <= 1)
    {
        std::cerr << "Try running this command with some arguments "
                  << "to see how it behaves." << std::endl
                  << "For example:" << std::endl
                  << argv[0] << " "
                  << "randomstring1 -s test -f 5.7 -f7.8 --float=4.1 -vv "
                  << "otherstring --verbose -vi 5 -i=2 -b1 -vbtrue -bfalse "
                  << "--double 1.08 -casdf -- -s \"a string\"" << std::endl
                  << std::endl;
    }

    /**************************************************************************
     * Create option parser and supply help messages.
     */
    OptionParser op(true, "This is an example of how to use OptionParser and "
                    "its capabilities.",
                    "An invokation example:\n"
                    "$ <command> -s test -f 5.7 -f7.8 --float=4.1 -vv "
                    "--verbose -vi 5 -i=2 -b1 -vbtrue -b=false --double 1.08");



    /**************************************************************************
     * Declare some basic example variables.
     */
    std::vector<std::string> strings;
    std::vector<int> integers;
    std::vector<float> floats;
    std::vector<bool> bools;
    CustomClass custom = {"default", 0};
    unsigned int verbosity(0);
    double doubles(0.0);



    /**************************************************************************
     * Adding options
     */
    op.add(new Option::Multiple<std::string>(&strings, "strings", 's',
                                             "Strings"));
    op.add(new Option::Multiple<int>(&integers, "integer", 'i', "Integers"));
    op.add(new Option::Multiple<float>(&floats, "float", 'f', "Floats"));
    op.add(new Option::Multiple<bool>(&bools, "bool", 'b', "Booleans"));
    op.add(new Option::Counter<unsigned int>(&verbosity, "verbose", 'v',
                                             "Level of verbosity"));
    op.add(new Option::Single<double>(&doubles, "double", 'd', "A double"));
    op.add(new Option::Single<CustomClass, true>(&custom, "custom", 'c',
                               "A custom struct (string + string length)"));



    /**************************************************************************
     * Automatically add any options for classes registered at the Factory to
     * OptionParser.
     */
    std::vector<CustomClass2*> custom2s;
    Factory<CustomClass2>::addClassOptions(&custom2s, &op);



    /**************************************************************************
     * Parse arguments. Pass "true" for pruning the argv/argc after parsing.
     * If the built-in help was invoked, the method returns false, abort.
     */
    if (!op.parse(argc, argv, true))
        return 0;



    /**************************************************************************
     * Printing values
     */

    std::cout << "strings (-s, --string):" << std::endl;
    for (size_t i = 0; i < strings.size(); i++)
        std::cout << " " << i << ": " << strings.at(i) << std::endl;

    std::cout << "integers (-i, --integer):" << std::endl;
    for (size_t i = 0; i < integers.size(); i++)
        std::cout << " " << i << ": " << integers.at(i) << std::endl;

    std::cout << "floats (-f, --float):" << std::endl;
    for (size_t i = 0; i < floats.size(); i++)
        std::cout << " " << i << ": " << floats.at(i) << std::endl;

    std::cout << "bools (-b, --bool):" << std::endl;
    for (size_t i = 0; i < bools.size(); i++)
        std::cout << " " << i << ": " << (bools.at(i) ? "true" : "false")
                  << std::endl;

    std::cout << "verbosity (-v, --verbose): " << verbosity << std::endl;
    std::cout << "double (-d --double): " << doubles << std::endl;
    std::cout << "custom struct (-c --custom): " << custom.s << " "
              << custom.l << std::endl;

    std::cout << "Remaining arguments:" << std::endl;
    for (int i = 0; i < argc; i++)
        std::cout << " " << i << ": " << argv[i] << std::endl;

    std::cout << "CustomClass2 (-2, --custom2):" << std::endl;
    for (size_t i = 0; i < custom2s.size(); i++)
        std::cout << " " << i << ": " << custom2s.at(i)->integer << std::endl;

    return 0;
}

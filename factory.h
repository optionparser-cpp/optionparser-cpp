/**
 * OptionParser Factory - 2013-04-22 - https://gitorious.org/optionparser-cpp
 *
 *   Factory addon for dynamically adding classes to a project.
 *   Usable both together with OptionParser and separately, with slightly
 *   different API:s.
 *
 * Copyright (C) 2013 Martin Haggstrom <https://gitorious.org/~mhn>
 *
 * Released under the Creative Commons Attribution-ShareAlike License
 *   (http://creativecommons.org/licenses/by-sa/3.0/)
 *
 **/

#ifndef FACTORY_H
#define FACTORY_H

#include <vector>
#include <cstring>

template <class T>
struct ClassInfo {

#ifdef OPTIONPARSER_CPP_H
    typedef void(*OptionAdder)(std::vector<T*>*, OptionParser*);
    OptionAdder f;
#else
    typedef T*(*Constructor)();
    Constructor f;
#endif
    const char* name;
};

template <class T>
class Factory
{
public:

#ifdef OPTIONPARSER_CPP_H
    typedef void(*OptionAdder)(std::vector<T*>*, OptionParser*);
#else
    typedef T*(*Constructor)();
#endif

    static int registerClass(
#ifdef OPTIONPARSER_CPP_H
            OptionAdder f,
            const char* name = NULL
#else
            Constructor f,
            const char* name = NULL
#endif
            )
    {
        ClassInfo<T>* newClasses = new ClassInfo<T>[numClasses+1];

        for (unsigned int i = 0; i < numClasses; i++)
            newClasses[i] = classes[i];

        newClasses[numClasses].name = name;
        newClasses[numClasses].f = f;

        delete classes;
        classes = newClasses;

        return numClasses++;
    }

#ifdef OPTIONPARSER_CPP_H
    static void addClassOptions(std::vector<T*>* v, OptionParser *op)
    {
        for (unsigned int i = 0; i < numClasses; i++)
            classes[i].f(v, op);
    }
#else
    static T* createClass(const char* name)
    {
        for (unsigned int i = 0; i < numClasses; i++)
            if (strcmp(classes[i].name, name) == 0)
                return classes[i].f();
        return NULL;
    }
#endif

    static std::vector<const char*> getClassNames()
    {
        std::vector<const char*> names;
        for (unsigned int i = 0; i < numClasses; i++)
            names.push_back(classes[i].name);
        return names;
    }

    static int getClassId(const char* name)
    {
        for (unsigned int i = 0; i < numClasses; i++)
            if (classes[i].name && strcmp(classes[i].name, name) == 0)
                return i;
        return -1;
    }

private:
    static unsigned int numClasses;
    static ClassInfo<T>* classes;
};

template <class T>
unsigned int Factory<T>::numClasses = 0;
template <class T>
ClassInfo<T>* Factory<T>::classes = 0;

#endif // FACTORY_H

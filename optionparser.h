/**
 * OptionParser - 2013-04-22 - https://gitorious.org/optionparser-cpp
 *
 *   A C++ library for parsing command line arguments the simplest way possible,
 *   with easy-to-use help and usage guidance features.
 *
 * Copyright (C) 2013 Martin Haggstrom <https://gitorious.org/~mhn>
 *
 * Released under the Creative Commons Attribution-ShareAlike License
 *   (http://creativecommons.org/licenses/by-sa/3.0/)
 *
 **/

#ifndef OPTIONPARSER_CPP_H
#define OPTIONPARSER_CPP_H

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>

#include "cstring"

namespace Option {

template <typename T>
inline T convertToType(std::string s, size_t)
{
    T obj;
    std::stringstream ss(s);
    ss >> obj;
    return obj;
}

template <>
inline bool convertToType<bool>(std::string s, size_t)
{
    for(unsigned int i = 0; i < s.size(); i++)
        s[i] = tolower(s[i]);

    if (s.compare("true") == 0)
        return true;
    if (s.compare("false") == 0)
        return false;
    bool obj;
    std::stringstream ss(s);
    ss >> obj;
    return obj;
}

class _OptionBase {
public:
    _OptionBase(const char short_opt, const char *long_opt, const char *msg,
               bool need_arg = false) :
        m_short(short_opt),
        m_long(long_opt),
        m_msg(msg),
        m_need_arg(need_arg)
    {}
    virtual ~_OptionBase() {}
    virtual void setValue(std::string s, size_t i) = 0;
    const char m_short;
    const char* m_long;
    const char* m_msg;
    bool m_need_arg;
};

template <typename T, bool mandatoryArgument = true, typename TI = T>
class Single : public Option::_OptionBase {
public:
    Single(TI *ptr, const char *long_opt, const char short_opt = ' ',
           const char *msg = NULL) :
        _OptionBase(short_opt, long_opt, msg, mandatoryArgument), m_ptr(ptr) {}
    virtual void setValue(std::string s, size_t i) { *m_ptr = convertToType<T>(s, i); }
    TI* m_ptr;
};

template <typename T, bool mandatoryArgument = true, typename TI = T>
class Multiple : public Option::_OptionBase {
public:
    Multiple(std::vector<TI> *ptr, const char *long_opt,
             const char short_opt = ' ', const char *msg = NULL) :
        _OptionBase(short_opt, long_opt, msg, mandatoryArgument), m_ptr(ptr) {}
    virtual void setValue(std::string s, size_t i) { m_ptr->push_back(convertToType<T>(s, i)); }
    std::vector<TI>* m_ptr;
};

template <typename T>
class Counter : public Option::_OptionBase {
public:
    Counter(T *ptr, const char *long_opt, const char short_opt = ' ',
            const char *msg = NULL) :
        _OptionBase(short_opt, long_opt, msg, false), m_ptr(ptr) {}
    virtual void setValue(std::string s, size_t i) {
        if (!s.empty()) *m_ptr = convertToType<T>(s, i);
        else (*m_ptr)++;
    }
    T* m_ptr;
};

}

/**
 * @brief The OptionParser class
 */

class OptionParser
{
public:

    /**
     * @brief OptionParser constructor creates an instance of OptionParser
     * @param builtinHelp Whether to use the built-in help function.
     * @param preHelp String to write in the beginning of built-in help message.
     * @param postHelp String to write in the end of built-in help message.
     */
    OptionParser(bool builtinHelp, const char *preHelp = NULL,
                 const char *postHelp = NULL) :
        longestLongOptn(0),
        printHelp(false),
        beforeHelp(preHelp),
        afterHelp(postHelp)
    {
        if (builtinHelp)
            m_options.push_back(new Option::Counter<bool>(
                                    &printHelp, "help", 'h',
                                    "Print a basic usage help."));
    }

    ~OptionParser()
    {
        for (unsigned int i = 0; i < m_options.size(); i++)
            delete m_options.at(i);
    }

    /**
     * @brief add Add an Option to the parser.
     * @param o An Option pointer.
     */
    void add(Option::_OptionBase *o)
    {
        if (o->m_long == NULL && o->m_short == ' ')
            return;

        unsigned int l;
        if (o->m_long != NULL && (l = strlen(o->m_long)) > longestLongOptn)
            longestLongOptn = l;

        m_options.push_back(o);
    }

    /**
     * @brief parse Parse the supplied argc/argv according to the previously
     *              added Option:s
     * @param argc The standard C/C++ argc
     * @param argv The standard C/C++ argv
     * @param prune Whether to prune the remaining argument list from
     *              recognized arguments
     * @return False if the built-in help is enabled and invoked, true
     *         otherwise.
     */
    bool parse(int &argc, char **&argv, bool prune = true)
    {
        int i(1);
        int restIdx(1);
        size_t currentArg(0);

        while(i < argc) {
            std::string arg(argv[i]);

            if (arg.length() > 2 && arg[0] == '-' && arg[1] == '-')
            {
                std::string param, param_arg;

                int found_idx = arg.find_first_of('=', 2);
                if (found_idx > 0)
                {
                    param_arg = arg.substr(found_idx+1);
                }

                param = arg.substr(2, found_idx > 0? found_idx-2: arg.length());

                for (size_t it = 0; it < m_options.size(); it++)
                {
                    Option::_OptionBase *o = m_options.at(it);
                    if (o->m_long != NULL && param.compare(o->m_long) == 0)
                    {
                        if (o->m_need_arg && found_idx < 0)
                        {
                            i++;
                            if (i < argc)
                            {
                                param_arg = argv[i];
                            }
                            else
                            {
                                std::cerr << "Warning: Option --" << o->m_long
                                          << " needs an argument." << std::endl;
                                break;
                            }
                        }
                        o->setValue(param_arg, currentArg++);
                        break;
                    }
                }
            }
            else if (arg.length() == 2 && arg[0] == '-' && arg[1] == '-')
            {
                if (prune)
                    for (i++; i < argc; i++)
                        argv[restIdx++] = argv[i];
                break;
            }
            else if (arg.length() > 1 && arg[0] == '-')
            {
                std::string param_arg;

                for (unsigned int c = 1; c < arg.length(); c++)
                {
                    char param = arg[c];

                    for (size_t it = 0; it < m_options.size(); it++)
                    {
                        Option::_OptionBase *o = m_options.at(it);
                        if (o->m_short == param)
                        {
                            if (o->m_need_arg)
                            {
                                if (c < arg.length() - 1)
                                {
                                    param_arg = arg.substr(c+1);
                                    if (param_arg[0] == '=')
                                        param_arg.erase(0, 1);
                                    c = arg.length();
                                }
                                else
                                {
                                    i++;
                                    if (i < argc)
                                        param_arg = argv[i];
                                    else
                                    {
                                        std::cerr << "Warning: Option -"
                                                  << o->m_short
                                                  << " needs an argument."
                                                  << std::endl;
                                        break;
                                    }
                                }
                            }
                            o->setValue(param_arg, currentArg++);
                            break;
                        }
                    }
                }
            }
            else if (prune)
            {
                argv[restIdx++] = argv[i];
            }

            i++;
        }

        if (prune)
            argc = restIdx;

        if (printHelp)
            return printHelpMessage();

        return true;
    }

private:

    /**
     * @brief printLine Prints a line to stdout, using smart line breaking.
     * @param line The string to print.
     * @param width The maximum width of the printed string.
     * @param lineBreak A sequence of charcters to write when breaking a line.
     */
    void printLine(const char *line, unsigned int width = 80,
                   const char *lineBreak = "\n")
    {
        const char* boundaries = " \n\t";
        const char* newlines = "\n";

        std::string str(line);

        bool first(true);
        while (str.length() > 0)
        {
            size_t idx = str.length();
            if (idx > width)
            {
                idx = str.find_first_of(newlines);
                if (idx == std::string::npos || idx > width)
                    idx = str.find_last_of(boundaries, width);
                if (idx == std::string::npos)
                    idx = width;
            }
            std::string line(str.substr(0, idx));
            str = str.substr(idx == str.length() ? idx : idx+1);
            if (!first)
                std::cout << lineBreak;
            else
                first = false;

            std::cout << line;
        }
    }

    /**
     * @brief printHelpMessage Prints a basic help message to stdout.
     * @return Unconditionally false.
     */
    bool printHelpMessage()
    {
        unsigned int width(80);
        unsigned int optlen = (longestLongOptn + 2 + 2 + 2 + 1 + 2);
        unsigned int msglen = width - optlen - 1;

        std::string fill(optlen + 1, ' ');
        fill[0] = '\n';

        if (beforeHelp != NULL)
        {
            printLine(beforeHelp, width);
            std::cout << std::endl << std::endl;
        }

        std::cout << "Options:" << std::endl;
        for (size_t i = 0; i < m_options.size(); i++)
        {
            std::stringstream opt;
            Option::_OptionBase *o = m_options.at(i);
            opt << " ";
            if (o->m_short != ' ')
                opt << "-" << o->m_short;
            else
                opt << "  ";
            if (o->m_short != ' ' && o->m_long != NULL)
                opt << ", ";
            else
                opt << "  ";
            if (o->m_long != NULL)
                opt << "--" << o->m_long;

            std::cout.flags(std::cout.flags() | std::cout.left);
            std::cout << std::setw(optlen) << opt.str();

            if (o->m_msg != NULL)
            {
                printLine(o->m_msg, msglen, fill.c_str());
            }
            std::cout << std::endl;
        }

        std::cout << std::endl;
        if (afterHelp != NULL)
        {
            printLine(afterHelp, width);
            std::cout << std::endl;
        }
        return false;
    }

private:

    std::vector<Option::_OptionBase*> m_options;
    unsigned int longestLongOptn;
    bool printHelp;
    const char *beforeHelp, *afterHelp;
};


#endif // OPTIONPARSER_CPP_H
